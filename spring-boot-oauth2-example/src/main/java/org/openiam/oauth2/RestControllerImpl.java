package org.openiam.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
public class RestControllerImpl {

    Logger logger = LoggerFactory.getLogger(RestControllerImpl.class);

    @GetMapping("/rest/api/me")
    public Map<String, Object> index(OAuth2AuthenticationToken authentication) {
        try {
            return authentication.getPrincipal().getAttributes();
        } catch (Exception e) {
            logger.error("Can't get data", e);
        }
        return Collections.emptyMap();
    }
}
