﻿using DemoWebApplication.Enteties;
using DemoWebApplication.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                var prop = new AuthenticationProperties { RedirectUri = "/" };
              //  prop.Dictionary.Add("prompt", "login");
                HttpContext.GetOwinContext().Authentication.Challenge(
                    prop,
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        /// <summary>
        /// Send an OpenID Connect sign-out request.
        /// </summary>
        public void SignOut(string post_logout_redirect_uri)
        {
            if (!Request.IsAuthenticated)
            {
                if (string.IsNullOrEmpty(post_logout_redirect_uri))
                {
                    Response.Redirect("/");
                }
                else
                {
                    Response.Redirect(post_logout_redirect_uri);
                }
            }
            else
            {
                HttpContext.GetOwinContext().Authentication.SignOut(
                    OpenIdConnectAuthenticationDefaults.AuthenticationType,
                    CookieAuthenticationDefaults.AuthenticationType);
            }
        }

        [Authorize]
        public ActionResult Search()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Search(UserSearchModel model)
        {
            string searchEndpoint = System.Configuration.ConfigurationManager.AppSettings["SearchEndpoint"];
            RestClient restClient = new RestClient(searchEndpoint);
            var result = await Request.GetOwinContext().Authentication.AuthenticateAsync("Cookies");
            string token = result.Properties.Dictionary["access_token"];
            restClient.Authenticator = new JwtAuthenticator(token);
            RestRequest restRequest = new RestRequest(Method.POST);
            SearchEntity searchEntity = new SearchEntity();
            searchEntity.lastName = model.LastName;
            restRequest.AddJsonBody(searchEntity);
            var restResponse = await restClient.ExecuteAsync<SearchUserResponse>(restRequest);
            ViewBag.UserList = restResponse.Data.beans;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}