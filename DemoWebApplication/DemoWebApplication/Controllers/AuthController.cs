﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoWebApplication.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult OpenIAMToken()
        {
            string access_token = Request.QueryString["access_token"];
            string id_token = Request.QueryString["id_token"];

            var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;
            string name = userClaims?.FindFirst("name")?.Value;
            return Redirect("/Home/Index");
        }
    }
}