﻿using System.Collections.Generic;

namespace DemoCoreWebApplication.Enteties
{
    public class SearchEntity
    {
        public SearchEntity()
        {
            roleIds = new List<string>();
            groupIds = new List<object>();
            organizationIds = new List<object>();
        }
        public int from { get; set; }
        public int size { get; set; }
        public string lastName { get; set; }
        public string principal { get; set; }
        public string email { get; set; }
        public List<string> roleIds { get; set; }
        public List<object> groupIds { get; set; }
        public List<object> organizationIds { get; set; }
        public string employeeId { get; set; }
        public bool fromDirectoryLookup { get; set; }
        public string sortBy { get; set; }
        public string orderBy { get; set; }
    }
}
