using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System.Threading.Tasks;

namespace DemoCoreWebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<OIDCConfig>(Configuration.GetSection(
                                        OIDCConfig.Position));
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
            })
      .AddCookie()
      .AddOpenIdConnect(options =>
      {
          options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
          options.Authority = Configuration["OIDCConfig:Authority"];
          options.RequireHttpsMetadata = true;
          options.ClientId = Configuration["OIDCConfig:ClientId"];
          options.ClientSecret = Configuration["OIDCConfig:ClientSecret"];
          options.ResponseType = OpenIdConnectResponseType.Code;
          options.GetClaimsFromUserInfoEndpoint = true;
          options.Scope.Clear();
          options.Scope.Add("openid");
          options.SaveTokens = true;
          options.TokenValidationParameters = new TokenValidationParameters
          {
              ValidateIssuer = true
          };
          options.Events.OnRedirectToIdentityProvider = context =>
          {
              context.ProtocolMessage.Prompt = "login";
              return Task.CompletedTask;
          };
          options.Events.OnRedirectToIdentityProviderForSignOut = context =>
          {
              context.Response.Redirect("/Home/Index");
              context.HandleResponse();
              return Task.CompletedTask;
          };
      });

            services.AddAuthorization();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
