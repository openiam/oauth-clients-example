﻿namespace DemoCoreWebApplication.Models
{
    public class OpenIAMUserInfoModel
    {
        public string id { get; set; }
        public string beanType { get; set; }
        public string operation { get; set; }
        public string classification { get; set; }
        public string middleInit { get; set; }
        public string accessRightIds { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string userStatus { get; set; }
        public string accountStatus { get; set; }
        public string principal { get; set; }
        public string organization { get; set; }
        public string department { get; set; }
        public string division { get; set; }
        public string title { get; set; }
        public string employeeId { get; set; }
        public string startDate { get; set; }
        public string accessRightStartDate { get; set; }
        public string accessRightEndDate { get; set; }
        public string organizations { get; set; }
        public string location { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string nickname { get; set; }
        public string birthdate { get; set; }
        public string birthdateAsStr { get; set; }
        public string maidenName { get; set; }
        public string suffix { get; set; }
        public string sex { get; set; }
        public string gender { get; set; }
        public string jobCodeId { get; set; }
        public string prefix { get; set; }
        public string prefixLastName { get; set; }
        public string partnerName { get; set; }
        public string locationCd { get; set; }
        public string userTypeInd { get; set; }
        public string employeeTypeId { get; set; }
        public string employeeTypeName { get; set; }
        public string costCenter { get; set; }
        public string alternateContactId { get; set; }
        public string alternativeStartDate { get; set; }
        public string alternativeEndDate { get; set; }
        public string certificationDelegateId { get; set; }
        public string certificationDelegateEndDate { get; set; }
        public string certificationDelegateStartDate { get; set; }
        public string supervisorMdTypeName { get; set; }
        public string accountTypeName { get; set; }
        public bool visible { get; set; }
        public string relationship { get; set; }
        public string mdTypeId { get; set; }
        public string applicationNames { get; set; }
        public string accessRightStartDateStr { get; set; }
        public string accessRightEndDateStr { get; set; }
    }
}
