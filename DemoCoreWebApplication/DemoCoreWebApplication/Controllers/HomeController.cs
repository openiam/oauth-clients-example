﻿using DemoCoreWebApplication.Enteties;
using DemoCoreWebApplication.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Authenticators;
using System.Diagnostics;
using System.Threading.Tasks;

namespace DemoCoreWebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly OIDCConfig _config;

        public HomeController(ILogger<HomeController> logger, IOptions<OIDCConfig> config)
        {
            _logger = logger;
            _config = config.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task Login(string returnUrl = "/")
        {
            await HttpContext.ChallengeAsync(OpenIdConnectDefaults.AuthenticationScheme, new AuthenticationProperties() { RedirectUri = returnUrl });
        }

        public async Task Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            await HttpContext.SignOutAsync(OpenIdConnectDefaults.AuthenticationScheme);
        }

        [Authorize]
        public IActionResult Secure()
        {
            return View();
        }

        [Authorize]
        public ActionResult Search()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Search(UserSearchModel model)
        {
            string searchEndpoint = _config.SearchEndpoint;
            RestClient restClient = new RestClient(searchEndpoint);
            var result = Request.Cookies;
            string token = await HttpContext.GetTokenAsync("access_token");
            restClient.Authenticator = new JwtAuthenticator(token);
            RestRequest restRequest = new RestRequest(Method.POST);
            SearchEntity searchEntity = new SearchEntity();
            searchEntity.lastName = model.LastName;
            restRequest.AddJsonBody(searchEntity);
            var restResponse = await restClient.ExecuteAsync<SearchUserResponse>(restRequest);
            ViewBag.UserList = restResponse.Data.beans;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
