﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenIAMAuth
{
    class AuthConfig
    {
        public AuthConfig()
        {
            RedirectUri = "http://localhost/testapp/AppLauncherRedirect";
            OpenIAMAddress = "https://aipoclouddemo.openiam.com";
            OAuth2Endpoint = "idp/oauth2/authorize";
        }
        public string RedirectUri { get; private set; }

        public string OpenIAMAddress { get; private set; }

        public string OAuth2Endpoint { get; private set; }

        public Uri AuthAuthorization()
        {
            UriBuilder uriBuilder = new UriBuilder(OpenIAMAddress);
            uriBuilder.Path = OAuth2Endpoint;
            return uriBuilder.Uri;
        }
    }
}
