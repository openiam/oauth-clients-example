﻿using OpenIAMAuth_WinForms;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsDemoApp.Enteties;

namespace WindowsFormsDemoApp
{
    public partial class Form1 : Form
    {
        TokenInfo _tokenInfo;
        AuthConfig _config;
        public Form1()
        {
            InitializeComponent();
        }

        private AuthConfig GetConfig()
        {
            //client_id and client_secret should be stored in secure place
            var client_id = ConfigurationSettings.AppSettings["client_id"];
            var client_secret = ConfigurationSettings.AppSettings["client_secret"];
            var openiamAddress = ConfigurationSettings.AppSettings["openiam_address"];
            return new AuthConfig(client_id, client_secret, openiamAddress);
        }

        private void button_Auth_Click(object sender, EventArgs e)
        {
            if(_config == null)
            {
                _config = GetConfig();
            }

            try
            {
                if (_tokenInfo == null)
                {
                    TokenProvider tokenProvider = new TokenProvider();
                    _tokenInfo = tokenProvider.GetToken(_config);
                }

                if (_tokenInfo != null)
                {
                    UriBuilder uriBuilder = new UriBuilder(_config.OpenIAMAddress);
                    uriBuilder.Path = "/idp/oauth2/userinfo";
                    RestClient client = new RestClient(uriBuilder.Uri);
                    client.RemoteCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;

                    RestRequest restRequest = new RestRequest();
                    restRequest.AddParameter("Authorization", "Bearer " + _tokenInfo.access_token, ParameterType.HttpHeader);
                    var response = client.Execute(restRequest);
                    textBox.Text += response.Content;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void searchButton_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(textBoxSearch.Text))
            {
                return;
            }
            if (_config == null)
            {
                _config = GetConfig();
            }
           
            try
            {
                if (_tokenInfo == null)
                {
                    TokenProvider tokenProvider = new TokenProvider();
                    _tokenInfo = tokenProvider.GetToken(_config);
                }

                RestClient restClient = new RestClient(_config.SearchUri());
                restClient.RemoteCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;
                restClient.Authenticator = new JwtAuthenticator(_tokenInfo.access_token);
                RestRequest restRequest = new RestRequest(Method.POST);
                SearchEntity searchEntity = new SearchEntity();
                searchEntity.lastName = textBoxSearch.Text;
                restRequest.AddJsonBody(searchEntity);
                var response = await restClient.ExecuteAsync<SearchUserResponse>(restRequest);
                
                foreach(var user in response.Data.beans)
                {
                    textBox.Text += Environment.NewLine;
                    textBox.Text += $"Id: {user.id}";
                    textBox.Text += Environment.NewLine;
                    textBox.Text += $"Principal: {user.principal}";
                    textBox.Text += Environment.NewLine;
                    textBox.Text += $"Name: {user.name}";
                    textBox.Text += Environment.NewLine;
                    textBox.Text += $"FirstName: {user.firstName}";
                    textBox.Text += Environment.NewLine;
                    textBox.Text += $"LastName: {user.lastName}";
                    textBox.Text += Environment.NewLine;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
