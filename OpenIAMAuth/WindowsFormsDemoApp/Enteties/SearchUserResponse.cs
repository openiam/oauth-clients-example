﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsDemoApp.Enteties
{
    class SearchUserResponse
    {
        public SearchUserResponse()
        {
            beans = new List<OpenIAMUserInfoModel>();
        }
        public object error { get; set; }
        public int page { get; set; }
        public int from { get; set; }
        public int size { get; set; }
        public int pageSize { get; set; }
        public List<OpenIAMUserInfoModel> beans { get; set; }
        public bool emptySearchBean { get; set; }
    }
}
