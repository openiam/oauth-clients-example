﻿using System;

namespace OpenIAMAuth_WinForms
{
    public class TokenProvider
    {
        public TokenInfo GetToken(AuthConfig config)
        {
            AuthBrowser authBrowser = new AuthBrowser();
            authBrowser.AuthConfig = config;
            var result = authBrowser.ShowDialog();
            if(result == System.Windows.Forms.DialogResult.OK)
            {
                return authBrowser.Token;
            }
            else
            {
                return null;
            }
        }
    }
}
