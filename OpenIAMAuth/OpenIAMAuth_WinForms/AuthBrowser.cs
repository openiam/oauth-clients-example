﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenIAMAuth_WinForms
{
    public partial class AuthBrowser : Form
    {
        public AuthBrowser()
        {
            InitializeComponent();
            webBrowser.IsWebBrowserContextMenuEnabled = false;
        }

        public AuthConfig AuthConfig { get; set; }

        public TokenInfo Token { get; private set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            webBrowser.Navigate(AuthConfig.AuthAuthorization());
        }
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (string.Equals(e.Url.Host, AuthConfig.RedirectUri.Host, StringComparison.OrdinalIgnoreCase))
            {
                var queries = GetQueries(e.Url);
                string code = queries["code"];
                string error = queries["error"];
                if(string.IsNullOrEmpty(error))
                {
                    RestClient client = new RestClient(AuthConfig.OAuth2TokenUri());
                    client.RemoteCertificateValidationCallback = (s, certificate, chain, sslPolicyErrors) => true;

                    RestRequest restRequest = new RestRequest(Method.POST);

                    restRequest.Parameters.Add(new Parameter("grant_type", "authorization_code", ParameterType.GetOrPost));
                    restRequest.Parameters.Add(new Parameter("code", code, ParameterType.GetOrPost));
                    restRequest.Parameters.Add(new Parameter("redirect_uri", AuthConfig.RedirectUri, ParameterType.GetOrPost));
                    restRequest.Parameters.Add(new Parameter("client_id", AuthConfig.ClientId, ParameterType.GetOrPost));
                    restRequest.Parameters.Add(new Parameter("client_secret", AuthConfig.ClientSecret, ParameterType.GetOrPost));
                    
                    //var content = restResponse.Content;
                    try
                    {
                        var restResponse = client.Execute<TokenInfo>(restRequest);
                        Token = restResponse.Data;
                        DialogResult = DialogResult.OK;
                        Close();
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = GetErrorMessage(ex.ToString());
                        webBrowser.Document.Write(errorMessage);
                    }
                }
                else
                {
                    string errorMessage = GetErrorMessage(error);
                    webBrowser.Document.Write(errorMessage);
                }
            }
        }

        private static string GetErrorMessage(string error)
        {
            string errorMessage = "<h1>Could not get code</h1>";
            errorMessage += $"<p>{error}</p>";
            return errorMessage;
        }

        public static NameValueCollection GetQueries( Uri uri)
        {
            var queryString = uri.Query.Substring(uri.Query.IndexOf('?')).Split('#')[0];
            var values = System.Web.HttpUtility.ParseQueryString(queryString);
            return values;
        }
    }
}
