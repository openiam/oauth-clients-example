﻿using System;

namespace OpenIAMAuth_WinForms
{
    public class AuthConfig
    {
        public AuthConfig(string clientId, string clientSecret, string openIAMAddress)
        {
            RedirectUri = new Uri("http://localhost/testapp");
            OpenIAMAddress = openIAMAddress;
            OAuth2Endpoint = "idp/oauth2/authorize";
            OAuth2TokenEndpoint = "idp/oauth2/token";
            ClientId = clientId;
            ClientSecret = clientSecret;
        }

        public Uri RedirectUri { get; private set; }

        public string OpenIAMAddress { get; private set; }

        public string OAuth2Endpoint { get; set; }

        public string OAuth2TokenEndpoint { get; set; }


        public string ClientId { get; private set; }
        public string ClientSecret { get; private set; }


        public Uri AuthAuthorization()
        {
            UriBuilder uriBuilder = new UriBuilder(OpenIAMAddress);
            uriBuilder.Path = OAuth2Endpoint;
            uriBuilder.Query = $"client_id={ClientId}&response_type=code&redirect_uri={RedirectUri}";
            return uriBuilder.Uri;
        }

        public Uri OAuth2TokenUri()
        {
            UriBuilder uriBuilder = new UriBuilder(OpenIAMAddress);
            uriBuilder.Path = OAuth2TokenEndpoint;
            return uriBuilder.Uri;
        }

        public Uri SearchUri()
        {
            UriBuilder uriBuilder = new UriBuilder(OpenIAMAddress);
            uriBuilder.Path = "/webconsole/rest/api/users/search";
            return uriBuilder.Uri;
        }
    }
}
