import {AuthConfig} from 'angular-oauth2-oidc';

export const authCodeFlowConfig: AuthConfig = {
  issuer: 'https://aipoclouddemo.openiam.com/idp/oauth2/angular-demo-implicit',

  redirectUri: window.location.origin + '/index.html',

  clientId: '40EF4426F5D3483D8B222DA7FD2F2DD9',
  dummyClientSecret: 'aebb786469bd877001119ac83ef7c8f72cef03e9c8e729deca7aaab96d44d747',
  responseType: 'id_token token',
  strictDiscoveryDocumentValidation: false,
  revocationEndpoint: 'https://aipoclouddemo.openiam.com/idp/oauth2/revoke',
  logoutUrl: 'https://aipoclouddemo.openiam.com/idp/logout',
  scope: 'email family_name given_name name preferred_username',
  showDebugInformation: true
};
