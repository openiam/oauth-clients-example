import {Component, OnInit} from '@angular/core';
import {authCodeFlowConfig} from './oauthConfig';
import {LoginOptions, OAuthService, UserInfo} from 'angular-oauth2-oidc';
import {formatWithOptions} from 'util';
import {HttpClient, HttpClientModule} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'oauth';
  header = 'Please login';
  accessToken = '';
  idToken = '';
  claims: any;

  constructor(private oauthService: OAuthService, private httpClient: HttpClient) {
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.loadDiscoveryDocument().then((doc) => {
      this.oauthService.tryLoginImplicitFlow().catch(err => {
        console.error(err);
      }).then(() => {
        if (!this.oauthService.hasValidAccessToken()) {
          this.oauthService.initImplicitFlow(null, {});
          sessionStorage.setItem('afterLogin', 'true');
        } else {
          if (sessionStorage.getItem('afterLogin') === 'true') {
            sessionStorage.removeItem('afterLogin');
            window.location.href = '/oauth/index.html';
          }
        }
      });
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.idToken = this.oauthService.getIdToken();
    this.accessToken = this.oauthService.getAccessToken();
    this.claims = this.oauthService.getIdentityClaims();
    this.header = `Hello ${this.claims.given_name} ${this.claims.family_name}`;
  }
}
